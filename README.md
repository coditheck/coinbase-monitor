# Coinbase Monitor

Track price for all balance :
`python3 coinbase-monitor.py --track -b -x0=5 -P=100 -wallets=all`

Get your balances :
`python3 coinbase-monitor.py --balance`