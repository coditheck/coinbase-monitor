#Importation
#############################################################

import Crypto,hashlib,os,sys,string
# from Crypto.Cipher import AES
# from Crypto.Cipher import ARC2
# from Crypto.Cipher import ARC4

import coinbase
from coinbase.wallet.client import Client

import requests
import json
import time
import datetime

#Graph
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
from matplotlib.widgets import Slider

import seaborn
seaborn.set() ;

#Alert
import tkinter as tk
from tkinter.messagebox import*
import pygame

#argparse
import argparse

##############################################################

matplotlib.use('TkAgg');

####definition pour le hash
def encrypt(message):
	return hashlib.pbkdf2_hmac( "sha224" , message.encode() , b'salt' , 10000000 ).hex() ;

#Get Keys
##############################################################
def getKeys() :
    with open('files/coinbaseKeys', 'a' , encoding="utf-8") as file:
        with open('files/coinbaseKeys', 'r') as file:
            text = file.read() ;
            file.close() ;

        file.close() ;

    with open('files/coinbaseKeys', 'a' , encoding="utf-8") as file:
        if( text == '' ) :
            api_key =  ( input( "Enter your api key : " ) ) ;
            api_secret = ( input( "Enter your api secret key : " ) ) ;

            file.write( api_key + " " + api_secret ) ;

            file.close() ;

        with open('files/coinbaseKeys', 'r' , encoding="utf-8") as file:
            text = file.read() ;
            file.close() ;

        file.close() ;

    return text ;

def changeKeys( apikey , secretkey ) :
    with open('files/coinbaseKeys', 'w' , encoding="utf-8") as file :
        file.write( apikey + " " + secretkey ) ;
        file.close() ;
    print( "keys changed with succefuly" ) ;

api_key = '' ;
api_secret = '' ;

##############################################################wallets

def playSong( audio ) :
	pygame.mixer.init() ;
	pygame.mixer.music.load( audio ) ;
	pygame.mixer.music.play( -1 ) ;
		
class Application( tk.Tk ) :
	def __init__( self , title , text ) :
		tk.Tk.__init__( self )
		self.create_widgets( title , text ) ;

	def create_widgets( self , title , text ) :
		self.label = tk.Label( self , text=text , pady=15 , padx=25 , font=( "", 14 ) ) ;
		self.label.grid( row=0 , columnspan=2 , column=0 )
		self.title( title ) ;

		self.grid( baseWidth=1 , baseHeight=1 , widthInc=1 , heightInc=90 ) ;
		self.bouton = tk.Button( self , text="OK" , command = self.ExitApp2 , pady=5 , padx=15 , fg="red" ) ;
		
		self.label.pack() ;
		self.bouton.pack() ;
		
		self.bind( "<Any-KeyPress>", self.ExitApp1 ) ;

	def ExitApp1( self , event ) :
		self.destroy() ;
		pygame.mixer.music.stop() ;

	def ExitApp2( self ) :
		self.destroy() ;
		pygame.mixer.music.stop() ;

class monitoring() :
    def __init__( self ) :
        self.client = None ;
        self.accounts = None ;
        self.user = None ;

        self.crypto = "" ;
        self.message = [] ;
        self.total = 0 ;
        self.currency = "" ;
        self.price = "" ;
        self.date = "" ;
        self.hour = "" ;
        self.minute = "" ;
        self.second = "" ;

        self.tabPrice = [] ;
        self.tabTime = [] ;

    ####################################################################################################
    #animation function
    def load_animation( self ) :
        #String to be displayed when the application is loading
        load_str = "loading..." ;
        ls_len = len( load_str ) ;

        #String for creating the rotating line
        animation = "|/-\\" ;
        #animation = "1-2-3-" ;
        anicount = 0 ;

        #used to keep the track of
        #the duration of animation
        counttime = 0 ;

        #pointer for travelling the loading string
        i = 0 ;

        while( counttime != 100 ) :
            #used to change the animation speed
            #smaller the value, faster will be the animation
            time.sleep( 0.1 ) ;

            #converting the string to list
            #as string is immutable
            load_str_list = list( load_str ) ;

            #x->obtaining the ASCII code
            x = ord( load_str_list[i] ) ;

            #y->for storing altered ASCII code
            y = 0 ;

            #if the character is "." or " ", keep it unaltered
            #switch uppercase to lowercase and vice-versa
            if( x != 32 and x != 46 ) :
                if( x > 90 ) :
                    y = x -32 ;

                else :
                    y = x + 32 ;

                load_str_list[ i ] = chr( y ) ;

            #for storing the resultant string
            res = '' ;
            for j in range( ls_len ) :
                res = res + load_str_list[ j ] ;

            #displaying the resultant string
            sys.stdout.write( "\r" + res + animation[ anicount ]  ) ;
            sys.stdout.flush() ;

            #Assigning loading string
            #to the resultant string
            load_str = res ;

            anicount = ( anicount + 1 ) % 4 ;
            i = ( i + 1 ) % ls_len ;
            counttime = counttime + 1 ;

        # #for windows os
        # if( os.name == "nt" ) :
            # os.system( "cls" ) ;

        # #for linux / Mac os
        # else :
            # os.system( "clear" ) ;

    ####################################################################################################
    #reload function
    def reload( self , boucle ) :
    	self.load_animation() ;
    	boucle = 0 ;

    ####################################################################################################
    #Make request function
    def makeRequest( self ) :
        boucle = 0 ;
        
        while boucle == 0 :
            try :
                self.client = Client(api_key, api_secret ) ;
                self.accounts = self.client.get_accounts() ;

                #assert isinstance(self.accounts.data, list)
                #assert self.accounts[0] is self.accounts.data[0]
                #assert len(self.accounts[::]) == len(self.accounts.data)
                #assert (self.accounts.warnings is None) or isinstance(self.accounts.warnings, list)
                #assert (self.accounts.pagination is None) or isinstance(self.accounts.pagination, dict)
                #self.accounts.refresh() ;
                #print( self.client.get_currencies() )
                
                #self.user = self.client.get_current_user() ;
                #print( self.client.get_time())
                #print( self.client.get_current_user() )
                #print( self.client.get_primary_account() ) ;
                
                #print( self.client.get_reports() ) ;
                boucle = 1 ;
                print( "\n\n" ) ;
                
            except coinbase.wallet.error.AuthenticationError:
                self.reload( boucle ) ;
    
            except requests.exceptions.ConnectionError:
                self.reload( boucle ) ;
    
            except simplejson.errors.JSONDecodeError:
                self.reload( boucle ) ;

        return 0 ;


    ###################################################################################################
    #Calc total balance of account
    def getTotalBalance( self ):

        self.makeRequest() ;
        self.message = [] ;
        
        total = 0 ;
        for wallet in self.accounts.data:
            value = str( wallet['native_balance']).replace('USD','');
            if( float( value ) != 0 ):
                self.message.append( 'Balance ' + str(wallet['currency']) + ' : ' + str(wallet['native_balance']) + ' => id :' + str(wallet['id']) );
                total += float(value);

        if( len( self.message ) == 0 ) :
            self.message.append( " no information getting " ) ;
 
        self.message.append( '\nTotal balance: ' + 'USD ' + str(total) ) ;
        self.total = total ;
        return total ;

    ###################################################################################################
    #Get specifique balance of account
    def getOnBalance( self , currency ) :
   
        self.makeRequest() ;
        self.message = [] ;
        value = 0 ;
        for wallet in self.accounts.data:
   
            if( str(wallet['currency']) == currency ):
                self.message.append( 'Balance ' + str(wallet['currency']) + ' : ' + str(wallet['native_balance']) + ' => id :' + str(wallet['id']) );
                value = float( str( wallet['native_balance']).replace('USD','') ) ;

        if( len( self.message ) == 0 ) :
             self.message.append( currency + " : none " ) ;

        return value ;
    
    ################################################################################################################
    #Print total balance of account
    def printBalance( self , wallets ) :
        if( wallets[0] == "all" ) :
            self.getTotalBalance() ;
            self.user = self.client.get_current_user() ;
            print( "\nname : " , self.user[ "name" ] ) ;
            print( "currency : " , self.user[ "native_currency" ] ) ;
            print( "\n---------------------------------------" ) ;
            print( ('\n').join( self.message ) );

        else :
            self.getTotalBalance() ;
            self.user = self.client.get_current_user() ;
            print( "\nname : " , self.user[ "name" ] ) ;
            print( "currency : " , self.user[ "native_currency" ] ) ;
            print( "\n---------------------------------------" ) ;
            for wallet in wallets :
                self.getOnBalance( wallet ) ;
                print( ('\n').join( self.message ) ) ;

    ################################################################################################################
    #track price
    def track( self , x0 , P , wallets ) :
        boucle = 0 ;
        pls = 0 ;
        loe = 5 ;
        i = 0 ;
        start_time = 0 ;
        currently_time = 0 ;
        times = 0 ;
        past_times = 0 ;
        pause = 60 ;
        self.tabTime = [] ;
        self.tabPrice = [0] ;
        lcolor = "" ;

        Pa = ( ( ( 100 + P )*10000 ) / ( 100 - loe )**2 ) - 100 ;

        print( "\n\nFor " + str( P ) + "% of " + wallets + " : you need " + str(Pa) + "%\n" ) ;

        if( wallets == "all" ) :

            while boucle == 0 :
                self.date = datetime.datetime.now() ;
                self.hour = self.date.hour * 3600 ;
                self.minute = self.date.minute * 60 ;
                self.second = self.date.second ;
                    
                if( currently_time == 0 ) :
                    times = 0 ;
                    self.tabTime.append( 0 ) ;
                    self.tabTime.append( currently_time ) ;
                    
                    currently_time = self.hour + self.minute + self.second ;
                    start_time = currently_time ;
                    
                else:
                    currently_time = self.hour + self.minute + self.second ;
                    past_times = times ;
                    times = currently_time - start_time ;
                    self.tabTime.append( times / 10 ) ;


                totalBalance = self.getTotalBalance() ;
                Pt = ( ( float(totalBalance) ) / ( x0*0.000001*(100-loe)**2 ) ) - 100 ;
                Pv = ( ( (100 + Pt)*( (100 - loe)**2 ) ) / 10000 ) - 100 ;
                
                y = float( Pt ) #+ x*0 ;
                self.tabPrice.append( y / 10 ) ;

                print( str(past_times) + "s to " + str(times) + "s => p(t)=" + str(  "{:.2f}%".format( y ) ) + \
                    ", P(t)=" + str( "{:.2f}%".format( Pv ) ) ) ;

                if self.tabPrice[ - 2 ] > self.tabPrice[ - 1 ] :
                    plt.plot( [ self.tabTime[ - 2 ] , self.tabTime[ - 1 ]  ] ,\
                        [ self.tabPrice[ - 2 ] , self.tabPrice[ - 1 ] ] , "y" ,\
                        marker="o", markersize=3 , color="red" , label=i ) ;
                    plt.text( self.tabTime[ - 1 ] , self.tabPrice[ - 1 ] ,\
                        "{:.2f}%".format(self.tabPrice[ - 1 ] * 10  ) ) ;                   

                elif self.tabPrice[ - 2 ] < self.tabPrice[ - 1 ] :
                    plt.plot( [ self.tabTime[ - 2 ] , self.tabTime[ - 1 ]  ] ,\
                        [ self.tabPrice[ - 2 ] , self.tabPrice[ - 1 ] ] , "y" ,\
                        marker="o", markersize=3 , color="green" , label=i ) ;
                    plt.text( self.tabTime[ - 1 ] , self.tabPrice[ - 1 ] ,\
                        "{:.2f}%".format(self.tabPrice[ - 1 ] * 10  ) ) ;

                else :
                    plt.plot( [ self.tabTime[ - 2 ] , self.tabTime[ - 1 ]  ] ,\
                        [ self.tabPrice[ - 2 ] , self.tabPrice[ - 1 ] ] , "y"\
                        ,  marker="o" , markersize=3 , color="yellow" ) ;

                if( Pt > 0 ):
                    lcolor = "green" ;
                elif( Pt == 0 ):
                    lcolor = "yellow" ;
                else:
                    lcolor = "red" ;

                legend = "p(t)=" + str(  "{:.2f}%".format( y ) ) + ", P(t)=" + str( "{:.2f}%".format( Pv ) ) ;
                
                patch = mpatches.Patch( color = lcolor , label=legend ) ;
                plt.legend( handles=[patch] , bbox_to_anchor=(0., 1.02, 1., .102 ) , borderaxespad=0 , loc='lower left' ) ;

                plt.xlim( self.tabTime[ - 2 ] - pause  , times/10 + 1 ) ;
                plt.subplot() ;
                plt.pause( pause );
                
                if Pt >= Pa and pls == 0 :
                    playSong( "sound/beep.ogg" ) ;

                    app = Application( "Alert" , "Vous avez atteint les " + str(P) + "%" ) ;
                    app.mainloop() ;

                    pls = 1 ;

                time.sleep( pause ) ;
                i = i + 3 ;

        else :
        
            while boucle == 0 :
                self.date = datetime.datetime.now() ;
                self.hour = self.date.hour * 3600 ;
                self.minute = self.date.minute * 60 ;
                self.second = self.date.second ;
                            
                if( currently_time == 0 ) :
                    times = 0 ;
                    self.tabTime.append( 0 ) ;
                    self.tabTime.append( currently_time ) ;
                    
                    currently_time = self.hour + self.minute + self.second ;
                    start_time = currently_time ;
                     
                else:
                    currently_time = self.hour + self.minute + self.second ;
                    past_times = times ;
                    times = currently_time - start_time ;
                    self.tabTime.append( times / 10 ) ;
        
        
                balance = self.getOnBalance( wallets ) ;
                Pt = ( ( float(balance) ) / ( x0*0.000001*(100-loe)**2 ) ) - 100 ;
                Pv = ( ( (100 + Pt)*( (100 - loe)**2 ) ) / 10000 ) - 100 ;
                        
                y = float( Pt ) #+ x*0 ;
                self.tabPrice.append( y / 10 ) ;
        
                print( wallets + " : " + str(past_times) + "s to " + str(times) + "s => p(t)=" + str(  "{:.2f}%".format( y ) ) + \
                    ", P(t)=" + str( "{:.2f}%".format( Pv ) ) ) ;
        
                if self.tabPrice[ - 2 ] > self.tabPrice[ - 1 ] :
                    plt.plot( [ self.tabTime[ - 2 ] , self.tabTime[ - 1 ]  ] ,\
                        [ self.tabPrice[ - 2 ] , self.tabPrice[ - 1 ] ] , "y" ,\
                        marker="o", markersize=3 , color="red" , label=i ) ;
                    plt.text( self.tabTime[ - 1 ] , self.tabPrice[ - 1 ] ,\
                        "{:.2f}%".format(self.tabPrice[ - 1 ] * 10  ) ) ;                   
        
                elif self.tabPrice[ - 2 ] < self.tabPrice[ - 1 ] :
                    plt.plot( [ self.tabTime[ - 2 ] , self.tabTime[ - 1 ]  ] ,\
                        [ self.tabPrice[ - 2 ] , self.tabPrice[ - 1 ] ] , "y" ,\
                        marker="o", markersize=3 , color="green" , label=i ) ;
                    plt.text( self.tabTime[ - 1 ] , self.tabPrice[ - 1 ] ,\
                        "{:.2f}%".format(self.tabPrice[ - 1 ] * 10  ) ) ;
        
                else :
                    plt.plot( [ self.tabTime[ - 2 ] , self.tabTime[ - 1 ]  ] ,\
                        [ self.tabPrice[ - 2 ] , self.tabPrice[ - 1 ] ] , "y"\
                        ,  marker="o" , markersize=3 , color="yellow" ) ;
        
                if( Pt > 0 ):
                    lcolor = "green" ;
                elif( Pt == 0 ):
                    lcolor = "yellow" ;
                else:
                    lcolor = "red" ;
        
                legend = wallets + " : p(t)=" + str(  "{:.2f}%".format( y ) ) + ", P(t)=" + str( "{:.2f}%".format( Pv ) ) ;
                        
                patch = mpatches.Patch( color = lcolor , label=legend ) ;
                plt.legend( handles=[patch] , bbox_to_anchor=(0., 1.02, 1., .102 ) , borderaxespad=0 , loc='lower left' ) ;
        
                plt.xlim( self.tabTime[ - 2 ] - pause  , times/10 + 1 ) ;
                plt.subplot() ;
                plt.pause( pause );
                        
                if Pt >= Pa and pls == 0 :
                    playSong( "sound/beep.ogg" ) ;
        
                    app = Application( "Alert" , "Vous avez atteint les " + str(P) + "% pour le " + wallets ) ;
                    app.mainloop() ;
        
                    pls = 1 ;
        
                time.sleep( pause ) ;
                i = i + 3 ;
                

####################################################################################################################


def interval( inf , sup ) :
    #inf += 1 ;
    while inf < sup :
        yield inf ;
        inf += 1 ;


monitoring = monitoring() ;

parser = argparse.ArgumentParser( description="coinbase-monitor") ;

parser.add_argument( "--track" , action="store_true" , help=" usage : --track -b -x0=float -P=float -wallets=all or --track -c lw list_wallets -lx0 list_x0 -lp list_P " ) ;
parser.add_argument( "--balance" , action="store_true" , help="usage : --balance -lw list_wallets or --balance -lw all" ) ;
parser.add_argument( "--changekeys" , action="store_true" , help="usage : --changekeys -apikeys=api_keys -secretkey=api_secret_key" ) ;

parser.add_argument( "-b" , action="store_true" , help="option b : track totale balance" ) ;
parser.add_argument( "-c" , action="store_true" , help="option c : track list balance" ) ;

parser.add_argument( "-x0" , type=float , help="Only number" ) ;
parser.add_argument( "-P" , type=float , help="Only number" ) ;
parser.add_argument( "-wallets" , type=str , help="Only string" ) ;

parser.add_argument( "-lw" , type=str , nargs="+" , help="List of currency" ) ;
parser.add_argument( "-lx0" , type=float , nargs="+" , help="List x0 value" ) ;
parser.add_argument( "-lP" , type=float , nargs="+" , help="List P value" ) ;

parser.add_argument( "-apikey" , type=str , help="Your apikey" ) ;
parser.add_argument( "-secretkey" , type=str , help="Your api secret key" ) ;

#parser.set_defaults( x0=0 , P=0 , wallets='all' ) ;

args = parser.parse_args() ;

if( args.track ) :
    keys = getKeys().split( " " ) ;
    #print( keys ) ;
    api_key = keys[ 0 ] ;
    api_secret = keys[ 1 ] ;

    if( args.b and not args.c ) :
        #print( args.x0 , args.P , args.wallets ) ;
        if( args.x0 and args.P and args.wallets and not args.lw and not args.lx0 and not args.lP ) :
            monitoring.track( args.x0 , args.P , args.wallets ) ;
        else:
            print( "get usage : coinbase-monitor -h" ) ;

    elif( args.c and not args.b ) :
        if( args.lw and args.lx0 and args.lP and not args.x0 and not args.P and not args.wallets ) :
            if( len( args.lw ) == len( args.lx0 ) and len( args.lw ) == len( args.lP ) ) :
                inter = interval( 0 , len( args.lw ) ) ;
                pidList = [] ;
                for n in inter :
                    pidList.append( os.fork() );
                    if( pidList[ -1 ] ) :
                        #print( "PID : " , pidList[ -1 ] ) ;
                        monitoring.track( args.lx0[ n ] , args.lP[ n ] , args.lw[ n ] ) ;
                        break ;

        else:
            print( "get usage : coinbase-monitor -h" ) ;

    else:
        print( "get usage : coinbase-monitor -h" ) ;

elif( args.balance and not args.b ) :
    keys = getKeys().split( " " ) ;
    api_key = keys[ 0 ] ;
    api_secret = keys[ 1 ] ;

    monitoring.printBalance( args.lw ) ;

elif( args.changekeys and args.apikey and args.secretkey ) :
    changeKeys( args.apikey , args.secretkey ) ;

else:
    print( "get usage : coinbase-monitor -h" ) ;